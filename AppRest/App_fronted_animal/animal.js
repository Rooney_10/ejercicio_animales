let animal ={
		id:0
}
function setIdAnimal(id){
			an.clave=id
		}


$(document).ready(inicio)
// Se inicializa la funcion para cargar los datos de la tabla de animales //;
function inicio() {
     /* Cargar los datos de la tabla de animal*/
	cargarDatos();
    /* Evento click con el boton guardar para el nuevo registro de la tabla animal */
    $("#guardarAnimal").click(guardar);
    /* Evento click con el boton eliminar  registro de  animal*/
    $("#eliminarAnimal").click(eliminar);
    /* Evento click para modificar del animal*/
   $("#modificarAnimal").click(modificar);

}

//metodo para cargar los datos con una peticion ajax en la tabla de especialdATOS CON UNA PETICION AJAX
//EN LA TABLA.
function cargarDatos() {
	// peticion AJAX al back-end
	$.ajax({
		url: "http://localhost:8080/animales/all",
		method: "Get",
		data: null,
		success: procesarDatosTabla,
		error:errorPeticion
	});
}
//procesando datos de la peticion	

		function procesarDatosTabla(response) {
			// se procesa la respuesta del back-end o servidor
			// y se agregar los valores obtenidos en la respuesta
			// a la tabla, a su tbody
			// resetear datos de la tabla
			$("#tDatos").html("");
			response.forEach(item => {
				$("#tDatos").append(""
				+"<tr>"
					+"<td><strong>"+""+item.clave+"<strong></td>"
					+"<td><strong>"+""+item.nombre+"<strong></td>"
					+"<td><strong>"+""+item.decripcion+"<strong></td>"
					+"<td><strong>"+""+item.tipo+"<strong></td>"
					+"<td><strong>"+""+item.fecha+"<strong></td>"
					+"<td>"
						+"<button onclick='preModificar("+item.clave+");' class='btn btn-warning ml-2'>Modificar</button>"
						+"<button onclick='preEliminar("+item.clave+");' class='btn btn-danger ml-2'>Eliminar</button>"
					+"</td>"
				+"</tr>"
				+"");
			});
		}
function errorPeticion(response) {
	alert("Error al realizar la peticion: " + response);
	console.log("Error al realizar la peticion: " + response);
}
//cargar datos en el mmodal de nuevo registro de especialidad//
function guardar(){
	$.ajax({
        url:"http://localhost:8080/animales/save",
        method:"Get",
        data:{
        	nombre:$("#nombre").val()
            descripcion:$("#descripcion").val()
            tipo:$("#tipo").val()
            fecha: $("#fecha").val(),
            
        },
        success:function(response){
	
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
//funcion preEliminar los datos en el mdoal del registro//
function preEliminar(id) {
	$("#id").val(id);
	$("#modalEliminarAnimal").modal();
  }
//funcion Eliminar los datos en el mdoal del registro//
  function eliminar() {
    $.ajax({
        url:"http://localhost:8080/animales/delete/"+id,
        method:"Post",
        data:null,
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
//funcion premodificar los datos en el mdoal del registro//
function preModificar(id) {
    $("#modalModificarAnimal").modal();
    // alert(id);
    $.ajax({
        url:"http://localhost:8080/animales/getAnimal/"+id,
        method:"Get",
        success:function(response){
            $("#id").val(response.id);
            nombre:$("#nombre2").val()
            descripcion:$("#descripcion2").val()
            tipo:$("#tipo2").val()
            fecha: $("#fecha2").val(),
        },
        error:errorPeticion

    });
}
//funcion para modificar el registro de animales//
function modificar() {
    var id=$("#id").val();
    $.ajax({
        url:"http://localhost:8080/animales/update/"+id,
        method:"Get",
        data:{
            id:id,
            nombre:$("#nombre2").val()
            descripcion:$("#descripcion2").val()
            tipo:$("#tipo2").val()
            fecha: $("#fecha2").val(),
        },
        success:function(response){
            alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion

    });
}
