package com.example.demo.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

//notacion para determinar que es una entidad//
@Entity
//se declara el nombre de la tabla//
@Table(name = "animales")
public class Animales {
	// se declaran las propiedades de la clase entidad//
	// llave primaria de la clase animales(clave)//
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	// declar la variable como varchar en la base de datos//
	@Column(nullable = false, columnDefinition = "VARCHAR(50)")
	private Long clave;
	// campo nombre//
	@NotNull
	@Column(nullable = false, columnDefinition = "VARCHAR(50)")
	private String nombre;
	// campo descripcion//
	@NotNull
	@Column(nullable = false, columnDefinition = "VARCHAR(200)")
	private String descripcion;
	// campo tipo//
	@NotNull
	@Column(nullable = false, columnDefinition = "VARCHAR(50)")
	private String tipo;
	//campo fecha de creacion//
	@Column(columnDefinition = "DATE DEFAULT CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaCreacion;

	// constructor vacio///
	public Animales() {

	}

	// constructores con variables inicializadas con llave primaria//
	public Animales(Long clave, String nombre, String descripcion, String tipo, Date fechaCreacion) {
		this.clave = clave;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tipo = tipo;
		this.fechaCreacion = fechaCreacion;
	}

	// constructores con variables inicializadas sin llave primaria//
	public Animales(String nombre, String descripcion, String tipo, Date fechaCreacion) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tipo = tipo;
		this.fechaCreacion = fechaCreacion;
	}
	//set y get//
	public Long getClave() {
		return clave;
	}

	public void setClave(Long clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
